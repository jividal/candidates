package ar.com.fluxit.candidates.spring.config;
import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
 

/**
 * 
 * @author Juan Ignacio Vidal
 * 
 * Configures Hibernate and persistences.
 *
 */
@Configuration
@EnableTransactionManagement
@ComponentScan("ar.com.fluxit.candidates.spring")
@PropertySource({ "classpath:persistence-mysql.properties" })
public class PersistenceConfig {
 
   @Autowired
   private Environment environment;
 
   /**
    * 
    * @author Juan Ignacio Vidal
    * 
    * Creates and return the session factory. Needs the configuration from restDataSource.
    * Indicates where the beans should be placed in order to be detected.
    */
   @Bean
   public LocalSessionFactoryBean sessionFactory() {
      LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
      sessionFactory.setDataSource(restDataSource());
      sessionFactory.setPackagesToScan(new String[] { "ar.com.fluxit.candidates.spring.model" });
      return sessionFactory;
   }
 
   /**
    * 
    * @author Juan Ignacio Vidal
    * Returns the datasource configured with the information obtained from the properties file.
    */
   @Bean
   public DataSource restDataSource() {
	   DriverManagerDataSource dataSource = new DriverManagerDataSource();
	   dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driverClassName"));
       dataSource.setUrl(environment.getRequiredProperty("jdbc.url"));
       dataSource.setUsername(environment.getRequiredProperty("jdbc.username"));
       dataSource.setPassword(environment.getRequiredProperty("jdbc.password"));
       
       return dataSource;
   }
   
   @SuppressWarnings("serial")
   Properties hibernateProperties() {
      return new Properties() {
         {
            setProperty("hibernate.hbm2ddl.auto", environment.getProperty("hibernate.hbm2ddl.auto"));
            setProperty("hibernate.dialect", environment.getProperty("hibernate.dialect"));
            setProperty("hibernate.globally_quoted_identifiers", "true");
         }
      };
   }
 
   /**
    * @author Juan Ignacio Vidal
    * @param the session factory
    * @return the transaction manager
    * 
    * Creates and return a new transaction manager, it needs the session factory.
    */
   @Bean
   @Autowired
   public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
      HibernateTransactionManager txManager = new HibernateTransactionManager();
      txManager.setSessionFactory(sessionFactory);
 
      return txManager;
   } 
}
