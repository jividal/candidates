package ar.com.fluxit.candidates.spring.dao;

import java.io.IOException;
import java.sql.Blob;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import ar.com.fluxit.candidates.spring.model.Person;

/**
 * Concrete implementation of PersonDAO.
 * @author Juan Ignacio Vidal
 *
 */
@Repository
public class PersonDAOImpl implements PersonDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(PersonDAOImpl.class);

	@Autowired(required=true)
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	/**
	 * Saves or updates the person to the database. Does not process the picture file.
	 * @author Juan Ignacio Vidal.
	 */
	@Override
	public void saveOrUpdatePerson(Person person) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(person);
		logger.info("Person saved/updated successfully, Person Details="+person);
	}

	/**
	 * Saves or updates the person to the database. Process the picture file, and converts it in a Blob.
	 * @author Juan Ignacio Vidal.
	 */
	@Override
	public void saveOrUpdatePerson(Person person, MultipartFile file) {
		if (file!=null&& !file.isEmpty()) {
			Session session = this.sessionFactory.getCurrentSession();
			try {
				Blob blob = Hibernate.getLobCreator(session).createBlob(file.getInputStream(), file.getSize());
				person.setImageFilename(file.getOriginalFilename());
				person.setImageContentType(file.getContentType());
				person.setImageContent(blob);
			} catch (IOException e) {
				logger.error("Error trying to get blob from file: " + file.getName() + "; while saving/updating person: " + person);
				e.printStackTrace();
			}
		}
		this.saveOrUpdatePerson(person);
		
	}
	
	/**
	 * Returns a list containing every person in the database.
	 * @author Juan Ignacio Vidal.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Person> listPersons() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Person> personsList = session.createQuery("from Person").list();
		for(Person p : personsList){
			logger.info("Person List::"+p);
		}
		return personsList;
	}

	/**
	 * Returns the person whose id match the parameter
	 * @author Juan Ignacio Vidal.
	 */
	@Override
	public Person getPersonById(int id) {
		Session session = this.sessionFactory.getCurrentSession();		
		Person p = (Person) session.load(Person.class, new Integer(id));
		logger.info("Person loaded successfully, Person details="+p);
		return p;
	}

	/**
	 * Removes the person whose id match the parameter
	 * @author Juan Ignacio Vidal.
	 */
	@Override
	public void removePerson(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Person p = (Person) session.load(Person.class, new Integer(id));
		if(null != p){
			session.delete(p);
		}
		logger.info("Person deleted successfully, person details="+p);
	}
}
