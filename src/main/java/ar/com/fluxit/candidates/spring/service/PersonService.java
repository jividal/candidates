package ar.com.fluxit.candidates.spring.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import ar.com.fluxit.candidates.spring.model.Person;

/**
 * PersonService interface. Defines the protocol to access to the person service.
 * @author Juan Ignacio Vidal
 *
 */
public interface PersonService {

	public List<Person> listPersons();
	public Person getPersonById(int id);
	public void removePerson(int id);
	public void saveOrUpdatePerson(Person person);
	public void saveOrUpdatePerson(Person person, MultipartFile file);
	
}
