package ar.com.fluxit.candidates.spring.dao;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import ar.com.fluxit.candidates.spring.model.Person;

/**
 * Interface for Data Access Objet to Person class.
 * @author Juan Ignacio Vidal
 *
 */
public interface PersonDAO {

	public List<Person> listPersons();
	public Person getPersonById(int id);
	public void removePerson(int id);
	public void saveOrUpdatePerson(Person p);
	public void saveOrUpdatePerson(Person person, MultipartFile file);
}
