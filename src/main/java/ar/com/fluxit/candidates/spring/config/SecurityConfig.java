package ar.com.fluxit.candidates.spring.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * 
 * @author Juan Ignacio VIdal
 * 
 * This a default caonfiguration to deploy an application with user permission and login.
 * It's a prototype, this should be defined with a database access information in the future.
 *
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
            .inMemoryAuthentication()
                .withUser("root").password("root2k16").roles("ADMIN");
    }
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
  
      http.authorizeRequests()
        	.antMatchers("/login*").permitAll() 
        	.antMatchers("/*").access("hasRole('ADMIN')")
        		.and()
        		.formLogin().loginPage("/login")
        					.defaultSuccessUrl("/persons", true)
        					.failureUrl("/loginError")
        		.and().logout().logoutUrl("/logout").logoutSuccessUrl("/");
      http.csrf().disable();
    }    
}
