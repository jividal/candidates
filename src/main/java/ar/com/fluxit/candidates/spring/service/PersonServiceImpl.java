package ar.com.fluxit.candidates.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import ar.com.fluxit.candidates.spring.dao.PersonDAO;
import ar.com.fluxit.candidates.spring.model.Person;


/**
 * Person service concrete implementation. Should define the bussiness logic, not yet defined by the domain.
 * @author Juan Ignacio Vidal
 *
 */
@Service
public class PersonServiceImpl implements PersonService {
	@Autowired(required=true)
	private PersonDAO personDAO;

	public void setPersonDAO(PersonDAO personDAO) {
		this.personDAO = personDAO;
	}

	@Override
	@Transactional
	public List<Person> listPersons() {
		return this.personDAO.listPersons();
	}

	@Override
	@Transactional
	public Person getPersonById(int id) {
		return this.personDAO.getPersonById(id);
	}

	@Override
	@Transactional
	public void removePerson(int id) {
		this.personDAO.removePerson(id);
	}
	

	@Override
	@Transactional
	public void saveOrUpdatePerson(Person person) {
		this.personDAO.saveOrUpdatePerson(person);
	}

	@Override
	@Transactional
	public void saveOrUpdatePerson(Person person, MultipartFile file) {
		if (file!=null && !file.isEmpty()) {
			this.personDAO.saveOrUpdatePerson(person, file);
		}
		else{
			this.saveOrUpdatePerson(person);
		}
		
	}

}
