package ar.com.fluxit.candidates.spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;


/**
 * 
 * @author Juan Ignacio Vidal
 *
 *	Configures file's uploading settings.
 *  MaxUploadSize and encoding should be read from a properties file.
 */
@Configuration
public class MultipartUploadConfig {

  @Bean
  public CommonsMultipartResolver multipartResolver() {
    CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
    multipartResolver.setMaxUploadSize(3145728);
    multipartResolver.setDefaultEncoding("utf-8");
    return multipartResolver;
  }
}
