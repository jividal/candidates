package ar.com.fluxit.candidates.spring;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 
 * @author Juan Ignacio Vidal
 * 		 Controller to manage Authentication related requests.
 *
 */
@Controller
public class AuthenticationController {
	
	/**
	 * 
	 * @author Juan Ignacio Vidal
	 * Login page request. May need to load information from services.
	 * 
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model) {
		return "login";
	}

	/**
	 * 
	 * @author Juan Ignacio Vidal
	 * Login Error page request. May need to load information from services.
	 * 
	 */
	@RequestMapping(value = "/loginError", method = RequestMethod.GET)
	public String loginError(Model model) {
		model.addAttribute("error", "true");
		model.addAttribute("errorMessage", "El usuario y/o contraseņa ingresados no son correctos.");
		return "login";
	}

}
