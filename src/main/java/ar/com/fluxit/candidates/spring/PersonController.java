package ar.com.fluxit.candidates.spring;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import ar.com.fluxit.candidates.spring.model.Person;
import ar.com.fluxit.candidates.spring.service.PersonService;

/**
 * 
 * @author Juan Ignacio Vidal
 * Controller to manage Person related requests.
 *
 */
@Controller
public class PersonController {
	@Autowired(required=true)
	private PersonService personService;
	
	
	@Qualifier(value="personService")
	public void setPersonService(PersonService ps){
		this.personService = ps;
	}
	
	/**
	 * 
	 * @author Juan Ignacio Vidal
	 * 
	 * Loads the response with the information needed to show the persons list
	 */
	@RequestMapping(value = "/persons", method = RequestMethod.GET)
	public String listPersons(Model model) {
		model.addAttribute("person", new Person());
		model.addAttribute("listPersons", this.personService.listPersons());
		return "personList";
	}
	
	/**
	 * @author Juan Ignacio Vidal
	 * 
	 * Sets a new person to the response, it may need load some extra information from some service.
	 */
	@RequestMapping(value = "/addPerson", method = RequestMethod.GET)
	public String addPersonForm(Model model) {
		//May be needed for loading some information from the server at the response.
		model.addAttribute("person", new Person());
		return "personForm";
	}
	
	
	/**
	 * 
	 * @author Juan Ignacio Vidal
	 * Saves or update the person loaded from the request. Redirects to the persons list.
	 */
	@RequestMapping(value= "/person/save", method = RequestMethod.POST)
	public String savePerson(@ModelAttribute("person") Person person, @RequestParam("file") MultipartFile file){
		personService.saveOrUpdatePerson(person, file);
		return "redirect:/persons";	
	}

	/**
	 * 
	 * @author Juan Ignacio Vidal
	 * Loads the person with the id specified in the path.
	 * Gets its image blob, and writes it in the response as an image file.
	 */	
	@RequestMapping("/imageDisplay/{id}")
	public void imageDisplay(@PathVariable("id") Integer personId, HttpServletResponse response,HttpServletRequest request) throws ServletException, IOException, SQLException{
	    Person person = personService.getPersonById(personId);        
	    writeImageInResponse(response, person);
	}

	/**
	 * 
	 * @author Juan Ignacio Vidal
	 * 
	 * Gets the person's image blob, and writes it in the response as an image file.
	 */	
	private void writeImageInResponse(HttpServletResponse response, Person person) throws IOException, SQLException {
		response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
	    response.getOutputStream().write(person.getImageContent().getBytes(1, (int)person.getImageContent().length()));
		response.getOutputStream().close();
	}
	
	
	/**
	 * 
	 * @author Juan Ignacio Vidal
	 * Removes the person loaded from the request. Redirects to the persons list. Person's id should exist in database. Needs validation.
	 */
	@RequestMapping("/remove/{id}")
    public String removePerson(@PathVariable("id") int id){
		
        this.personService.removePerson(id);
        return "redirect:/persons";
    }
 
	/**
	 * 
	 * @author Juan Ignacio Vidal
	 * Loads the person corresponding to the request id, and sets it into the response.  Person's id should exist in database. Needs validation.
	 */
    @RequestMapping(value="/viewDetails", method = RequestMethod.GET)
    public String editPerson(@RequestParam("id") int id, Model model){
        model.addAttribute("person", this.personService.getPersonById(id));
		return "personDetails";
    }
	
}
