package ar.com.fluxit.candidates.spring.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

/**
 * Initializes Spring Dispacher Servlet in Servlet 3.0+ environment.
 * 
 * @author Juan Ignacio Vidal
 *
 */
public class SpringWebAppInitializer implements WebApplicationInitializer {

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
        ServletRegistration.Dynamic dispatcher = servletContext.addServlet("SpringDispatcher", new DispatcherServlet(this.getAppContext()));
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping("/");
	}
	
	public AnnotationConfigWebApplicationContext getAppContext(){
		AnnotationConfigWebApplicationContext appContext = new AnnotationConfigWebApplicationContext();
        appContext.register(MvcConfig.class);
        appContext.register(PersistenceConfig.class);
        appContext.register(MultipartUploadConfig.class);
        return appContext;
	}

}