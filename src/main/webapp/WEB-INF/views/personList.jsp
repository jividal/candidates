<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<html>
<head>
	<title>Candidatos</title>
	<style type="text/css">
		.tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:none;border-bottom-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff; text-align: center;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:bold;padding:10px 5px;border-style:none;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
		.tg .tg-4eph{background-color:#f9f9f9}
	</style>
	<style type="text/css">
		h1{
			font-family: Arial;
		}
		p, a { width: 50%;
			font-family: Arial;
		}
		img {
			float: left;
			padding-right: 15px;
			width: 90px;
			height:90px ;
		}
	</style>
</head>
<body>
<h1>Candidatos</h1>
<br/>

<c:if test="${!empty listPersons}">
	<table class="tg">
	<tr>
		<th width="80">Foto</th>
		<th>Nombre y Apellido</th>
		<th width="60">DNI</th>
		<th width="60">Detalles</th>
		<th width="60">Eliminar</th>
	</tr>
	<c:forEach items="${listPersons}" var="person">
		<tr>
			<td><img src="imageDisplay/${person.id}"/></td>
			<td>${person.fullName}</td>
			<td>${person.DNI}</td>
			<td><a href="<c:url value='/viewDetails?id=${person.id}' />" >Detalles</a></td>
			<td><a href="<c:url value='/remove/${person.id}' />" >Eliminar</a></td>
		</tr>
	</c:forEach>
	</table>
</c:if>
<br/>
<br/>
<a href="<c:url value='/addPerson'/>">Agregar un Candidato</a>
<a href="<c:url value='/logout'/>">Finalizar Sesi�n</a>

</body>
</html>
