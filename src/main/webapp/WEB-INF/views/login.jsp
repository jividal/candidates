<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html>

<head>
<title>Candidatos - Iniciar Sesi�n</title>
<style type="text/css">
		h1, h3{
			font-family: Arial;
		}
		h3.errorMessage{
			color: red;
		}
		p, label, a { width: 50%;
			font-family: Arial;
		}
		input {
			width:250px;
		}
		td{
			padding-bottom: 15px;
		}
		td.label{
			text-align: right;
			padding-right: 10px;
		}
	</style>
</head>
<body>
	<h1>Iniciar Sesi�n</h1>
	<c:if test="${error}">
		<h3 class="errorMessage">
			<c:out value="${errorMessage}"/>
		</h3>
	</c:if>
	<br/>
	<br/>
	<form name='loginForm' action="<c:url value='login' />" method='POST'>
		<table>
			<tr>
				<td class="label">
					<label>
						<spring:message text="Usuario"/>
					</label>
				</td>
				<td><input type='text' name='username' value=''></td>
			</tr>
			<tr>
				<td class="label">
					<label >
						<spring:message text="Contrase�a"/>
					</label>
				</td>
				<td><input type='password' name='password' /></td>
			</tr>
			<tr>
				<td>
				</td>
				<td>
					<br/>
					<br/>
					<input name="submit" type="submit" value="Ingresar" />
				</td>
			</tr>
		</table>
	</form>
</body>
</html>
