<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page session="false" %>
<html>
<head>
	<title>Candidato - <c:out value="${person.fullName}"/></title>
	<style type="text/css">
		h1{
			font-family: Arial;
		}
		p,a { width: 50%;
			font-family: Arial;
		}
		img {
			float: left;
			padding-right: 15px;
			width: 225px;
			height:225px ;
		}
	</style>
</head>
<body>
<h1>
	Candidato - <c:out value="${person.fullName}"/>
</h1>
<div style="width:100%;">
<img src="imageDisplay/${person.id}"/><br/>
<p><b>DNI: </b><c:out value="${person.DNI}"/></p>
<p><b>Fecha de Nacimiento: </b><fmt:formatDate value="${person.birthDate}" type="both" 
      pattern="dd/MM/yyyy" /></p>
<p><b>Direcci�n: </b><c:out value="${person.address}"/></p>
<p><b>N�mero de Tel�fono: </b><c:out value="${person.phoneNumber}"/></p>
<p><b>Correo Electr�nico: </b><c:out value="${person.eMail}"/></p>
<br/>
<p><b>Experiencia Laboral: </b><c:out value="${person.workExperience}"/></p>
</div>
<br/>
<br/>

<a href="<c:url value='/persons'/>" >Ver otros candidatos</a>
<a href="<c:url value='/logout'/>">Finalizar Sesi�n</a>

</body>
</html>
