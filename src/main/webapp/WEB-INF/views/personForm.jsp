<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<html>
<script>
function validateSubmit(){
	var formHasErrors=false
	clearErrors("birthDateErrorMessage");
	clearErrors("eMailErrorMessage");
	clearErrors("fileErrorMessage");
	if(!isValidDate()){
		showError(invalidDateError(), "birthDateErrorMessage");
		formHasErrors=true;
	}
	if(!isValidEmail()){
		showError(invalidEmailError(), "eMailErrorMessage");
		formHasErrors=true;
	}
	if(!isValidFile()){
		showError(invalidFileError(), "fileErrorMessage");
		formHasErrors=true;
	}
	if(!formHasErrors){
		document.forms[0].submit();
	}
};

function clearErrors(errorMessageNodeId){
	var errorNode = document.getElementById(errorMessageNodeId);
	while (errorNode.firstChild) {
	    errorNode.removeChild(errorNode.firstChild);
	}
}
function invalidDateError(){
	return "La fecha de nacimiento ingresada es inv�lida. Respete el formato DD/MM/AAAA.";
};
function invalidFileError(){
	return "Por favor seleccione una archivo para la imagen del candidato.";
};
function invalidEmailError(){
	return "El correo electr�nico ingresado es inv�lido.";
};
function showError(errorMessage, errorMessageNodeId){
	var errorNode = document.getElementById(errorMessageNodeId);
	errorNode.appendChild(document.createTextNode(errorMessage));
};

//Validates that a file is setted
function isValidFile(){
	return document.getElementById("file").files.length!=0;
};

// Validates that, if setted, the email is valid.
function isValidEmail(){
	var eMail = document.getElementById("eMail").value;
	return eMail == "" || (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(eMail));
};

// Validates that the setted date has the format "dd/mm/yyyy"
function isValidDate(){
	var dateString = document.getElementById("birthDate").value;
	if(dateString == "") return false;
	// Checking the pattern
    if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString)) return false;

    // Parsing to integers
    var parts = dateString.split("/");
    var day = parseInt(parts[0], 10);
    var month = parseInt(parts[1], 10);
    var year = parseInt(parts[2], 10);

    // Checking ranges of month and year
    if (year < 1900 || year > 2500 || month == 0 || month > 12) return false;

    var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

    // Adjust for leap years
    if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) monthLength[1] = 29;

    // Checking range of the day
    return day > 0 && day <= monthLength[month - 1];
};


</script>
<head>
	<title><spring:message text="Agregar"/></title>
	<style type="text/css">
		h1, div{
			font-family: Arial;
		}
		p, label, a { width: 50%;
			font-family: Arial;
		}
		img {
			float: left;
			padding-right: 15px;
			width: 225px;
			height:225px ;
		}
		input {
			width:250px;
		}
		textarea {
			width:250px;
		}
		td{
			padding-bottom: 15px;
		}
		td.label{
			text-align: right;
			padding-right: 10px;
		}
		div.errorMessage{
			color: red;
		}
	</style>
</head>
<body>
<h1>
	Agregar un candidato
</h1>

<br/>
<br/>


<c:url var="addAction" value="/person/save" ></c:url>

<form:form action="${addAction}" commandName="person" enctype="multipart/form-data" method="POST" acceptCharset="utf-8">
<table>
	<tr>
		<td class="label">
			<form:label path="fullName">
				<spring:message text="Nombre y Apellido"/>
			</form:label>
		</td>
		<td>
			<form:input path="fullName" />
		</td> 
	</tr>
	<tr>
		<td class="label">
			<form:label path="DNI">
				<spring:message text="DNI"/>
			</form:label>
		</td>
		<td>
			<form:input path="DNI" />
		</td>
		
	</tr>
	<tr>
		<td class="label">
			<form:label path="birthDate">
				<spring:message text="Fecha de nacimiento"/>
			</form:label>
		</td>
		<td>
			<form:input path="birthDate"  id="birthDate"/>
		</td>
		<td>
			<div class="errorMessage" id="birthDateErrorMessage"></div>
		</td>
	</tr>
	<tr>
		<td class="label">
			<form:label path="address">
				<spring:message text="Domicilio"/>
			</form:label>
		</td>
		<td>
			<form:textarea path="address" />
		</td>
		
	</tr>
	<tr>
		<td class="label">
			<form:label path="phoneNumber">
				<spring:message text="Tel�fono"/>
			</form:label>
		</td>
		<td>
			<form:input path="phoneNumber" />
		</td>
	</tr>
	<tr>
		<td class="label">
			<form:label path="eMail">
				<spring:message text="Correo Electr�nico"/>
			</form:label>
		</td>
		<td>
			<form:input path="eMail" id="eMail"/>
		</td>
		<td>
			<div class="errorMessage" id="eMailErrorMessage"></div>
		</td>
	</tr>
	<tr>
		<td class="label">
			<form:label path="workExperience">
				<spring:message text="Experiencia Laboral"/>
			</form:label>
		</td>
		<td>
			<form:textarea path="workExperience" />
		</td>
	</tr>
	<tr>
		<td class="label">
			<form:label path="imageFilename">
				<spring:message text="Imagen"/>
			</form:label>
		</td>
		
		<td>
			<input type="file" name="file" id="file"></input>
		</td>
		<td>
			<div class="errorMessage" id="fileErrorMessage"></div>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<br/>
			<br/>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<input type="button" value="<spring:message text="Agregar"/>" onclick="validateSubmit();" style="width:150px;"/>
		</td>
	</tr>
</table>	
</form:form>
<br/>
<br/>

<a href="<c:url value='/persons'/>">Volver al listado</a>
<a href="<c:url value='/logout'/>">Finalizar Sesi�n</a>

</body>
</html>
