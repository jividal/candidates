# README #

FluxIt Candidates. A demo application for a technical evaluation.

### What is this repository for? ###

* This repository was created and is mantained in order to store the sources for a demo application for a technical evaluation.
* Version 1.0

### Requirements ###
* Maven 3+.
* MySQL 5+. (Tested on Mysql 5, may run in older versions).

### How do I get set up? ###
* Clone the repository.
* Run the MySQL script placed in DBResources/candidates.sql. (Your mysql port should be 3306)
* Go to your cloned repository path in a console and execute: 
```
mvn tomcat7:run.
```
* Open a browser and address localhost:8080/Candidates.
* Application's user is root, and password is root2k16.

### Contribution guidelines ###

Any critics or comments will be welcomed.

### Who do I talk to? ###

Juan Ignacio Vidal (vidal.juanignacio@gmail.com)